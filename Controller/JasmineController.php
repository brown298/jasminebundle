<?php
namespace Brown298\JasmineBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class JasminController
 *
 * @package Brown298\JasmineBundle\Controller
 * @route("/jasmine")
 */
class JasmineController extends Controller
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @route("/run", name="jasmine_run")
     */
    public function runAction()
    {

        return $this->render('Brown298JasmineBundle:Jasmine:run.html.twig', array( ));
    }

}