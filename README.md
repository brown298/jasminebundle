# Symfony2 extension to test JavaScript with the Jasmine Testing Framework

## Config ##

Add the bundle to the dev section of 'AppKernel.php'


```
#!php

 if (in_array($this->getEnvironment(), array('dev', 'test'))) {
     $bundles[] = new Brown298\JasmineBundle\Brown298JasmineBundle();
 }

```

Add the routing file to routing_dev.yml


```
#!yml

_jasmine:
    resource: "@Brown298JasmineBundle/Resources/config/routing.yml"

```

Configure assetic to include the files you would like to test.

```
#!ymll

assetic:
  bundles: [Brown298JasmineBundle]
  assetic:
    jasmine:
      input:
        - "<path to file to test>"
        - "<path to test suite>"
```

Run the tests by visiting '/app_dev.php/jasmine/run'